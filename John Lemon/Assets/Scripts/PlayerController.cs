using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    
    private int count;
    
    public Text countText;
   
    // Start is called before the first frame update
    void Start()
    {
        SetMoneyCount();
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetMoneyCount()
    {
        countText.text = "Money: $" + count.ToString() + ".00";
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 150;

            SetMoneyCount();
        }
        
    }
}
